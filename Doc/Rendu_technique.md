Voici un document technique pour le programme:

---

# Système Solaire - Document Technique

## Introduction
Le programme "Système Solaire" est une simulation interactive du système solaire. Il utilise la bibliothèque Pygame pour créer une interface graphique et afficher les différentes planètes ainsi que d'autres objets célestes.

## Architecture Globale
Le programme est divisé en plusieurs parties principales :
1. **Initialisation et Configuration**: Cette partie comprend l'importation des bibliothèques nécessaires, la définition des constantes telles que les couleurs et les paramètres des planètes, ainsi que la création de la fenêtre d'affichage.
2. **Classe ShootingStar**: Cette classe représente une étoile filante dans le système solaire. Elle contient les attributs et les méthodes nécessaires pour gérer le mouvement de l'étoile filante.
3. **Fonctions Utilitaires**: Ces fonctions comprennent la génération d'étoiles filantes aléatoires, le dessin des étoiles filantes, le mouvement des étoiles filantes, etc.
4. **Fonctions Principales**: Ces fonctions gèrent le dessin des orbites des planètes, le dessin des objets célestes, la gestion du temps, etc.
5. **Interface Utilisateur (GUI)**: Cette partie gère l'interface utilisateur à l'aide de la bibliothèque Tkinter. Elle comprend le menu principal, les boutons d'action, etc.
6. **Boucle Principale**: La boucle principale du programme qui met à jour l'affichage, gère les événements utilisateur et maintient le programme en cours d'exécution.

## Principales Fonctionnalités
1. **Affichage du Système Solaire**: Le programme affiche les planètes du système solaire en fonction de leurs positions orbitales.
2. **Contrôle du Temps**: L'utilisateur peut accélérer, ralentir ou mettre en pause la simulation.
3. **Interaction Utilisateur**: L'utilisateur peut cliquer sur une planète pour afficher des informations détaillées à son sujet.
4. **Interface Utilisateur Intuitive**: Le programme dispose d'un menu principal convivial avec des boutons pour démarrer la simulation, afficher des informations et des crédits.

## Dépendances
Le programme utilise les bibliothèques suivantes :
- Pygame: Pour la création de l'interface graphique et la gestion des événements.
- Tkinter: Pour la création de l'interface utilisateur (menu principal, boutons, etc.).

## Exécution du Programme
Pour exécuter le programme, exécutez le fichier contenant le code principal (`solar_system.py`). Cela ouvrira le menu principal où vous pourrez démarrer la simulation, afficher des informations ou des crédits.

## Limitations et Améliorations Futures
Bien que le programme offre une simulation de base du système solaire, il existe des domaines où il pourrait être amélioré :
- Ajout de plus de détails et de fonctionnalités pour chaque planète.
- Amélioration de l'interface utilisateur avec des fonctionnalités supplémentaires telles que le zoom, la rotation de la caméra, etc.
- Optimisation des performances pour gérer un plus grand nombre d'objets célestes et améliorer la fluidité de l'animation.

## Conclusion
Le programme "Système Solaire" est une application interactive qui offre aux utilisateurs une expérience de simulation du système solaire. Il combine des fonctionnalités de simulation physique, d'interface utilisateur conviviale et d'interactivité pour offrir une expérience immersive. Ce document technique fournit une vue d'ensemble du programme, de son architecture et de ses fonctionnalités, ainsi que des suggestions pour des améliorations futures.
