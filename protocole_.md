# AstroPy - Système Solaire en Python

AstroPy est une simulation du système solaire réalisée en Python avec l'utilisation de la bibliothèque Pygame. Ce projet vise à fournir une représentation visuelle et interactive du système solaire, permettant aux utilisateurs d'explorer les planètes et d'obtenir des informations sur chacune d'elles.

## Installation prérequise

Pour exécuter le programme AstroPy, vous aurez besoin des bibliothèques Python suivantes, qui peuvent être installées via pip, le gestionnaire de paquets Python :

- **Pygame** : Pygame est une bibliothèque de jeu multiplateforme destinée au développement de jeux multimédias en Python. Elle est utilisée dans ce projet pour créer la simulation graphique du système solaire. Vous pouvez l'installer en utilisant la commande suivante :
pip install pygame

vbnet
Copy code

- **Tkinter** : Tkinter est une bibliothèque graphique standard de Python, souvent utilisée pour créer des interfaces utilisateur (UI) graphiques. Dans ce projet, Tkinter est utilisé pour créer un menu principal interactif. Tkinter est normalement inclus dans les distributions Python standard, donc vous ne devriez pas avoir besoin de l'installer séparément. Cependant, si vous rencontrez des problèmes avec Tkinter, assurez-vous d'installer le package tkinter approprié pour votre système.
- Sur Ubuntu/Debian, vous pouvez l'installer avec :
  ```
  sudo apt-get install python3-tk
  ```
- Sur d'autres distributions Linux, le nom du paquet peut varier, mais il sera généralement préfixé par "python3-" suivi de "tk".

Assurez-vous d'exécuter ces commandes dans votre terminal ou votre invite de commande après avoir installé Python sur votre système. Une fois ces bibliothèques installées, vous devriez être prêt à exécuter le programme AstroPy sans problème.

## Fonctionnalités

- Simulation en temps réel du mouvement des planètes autour du Soleil.
- Possibilité de ralentir, accélérer ou mettre en pause le temps.
- Affichage des orbites des planètes pour une meilleure compréhension des trajectoires.
- Génération aléatoire d'étoiles filantes pour ajouter à l'esthétique globale.
- Informations détaillées disponibles sur chaque planète.

## Instructions d'Installation

Assurez-vous d'avoir Python installé sur votre système. Si ce n'est pas le cas, téléchargez et installez-le à partir du [site officiel de Python](https://www.python.org/).

## Utilisation

Après avoir exécuté le programme, vous serez accueilli par le menu principal où vous pouvez démarrer la simulation du système solaire en cliquant sur le bouton "Start". Vous pouvez également accéder aux informations sur le projet en cliquant sur le bouton "Information", ou consulter les crédits en cliquant sur le bouton "Credit".

Une fois dans la simulation, vous pouvez interagir avec les planètes en utilisant les touches fléchées pour ralentir (flèche vers le bas) ou accélérer (flèche vers le haut) le temps. Vous pouvez également mettre en pause le temps en appuyant sur la barre d'espace. En cliquant sur une planète pendant que le temps est en pause, des informations détaillées sur la planète seront affichées.