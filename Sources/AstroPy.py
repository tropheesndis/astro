# -*- coding: utf-8 -*-
import pygame
import math
import random
import tkinter as tk
from tkinter import messagebox

# Initialisation de Pygame
pygame.init()

# Définition des couleurs
WHITE = (255, 255, 255)
YELLOW = (255, 255, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
ORANGE_TO = (255, 69, 0)
ORANGE_T1 = (255, 50, 0)
YELLOW2 = (255, 200, 0)
BROWN = (165, 42, 42)  # Couleur brune pour Saturne

# Paramètres de la fenêtre
WIDTH, HEIGHT = 1920, 1080
FPS = 60

# Paramètres du soleil
SUN_RADIUS = 60
SUN_POS = [WIDTH // 2, HEIGHT // 2]
# Chemin de l'image de texture du soleil
SUN_TEXTURE_PATH = "Images/sun.png"

# Paramètres de la Terre
EARTH_RADIUS = 16
# Chemin de l'image de texture de la Terre
EARTH_TEXTURE_PATH = "Images/terre.png"

# Chemins des images de texture des autres planètes
PLANET_TEXTURE_PATHS = {
    "Mercure": "Images/mercure.png",
    "Venus": "Images/venus.png",
    "Mars": "Images/mars.png",
    "Jupiter": "Images/jupiter.png",
    "Saturne": "Images/saturne.png",
    "Uranus": "Images/uranus.png",
    "Neptune": "Images/neptune.png"
}

# Paramètres des planètes (ordre : Vénus, Terre, Mars)
PLANET_DATA = [
    {"name": "Sun", "radius": 60, "distance": 0, "color": YELLOW, "speed": 0},
    {"name": "Mercure", "radius": 15, "semi_major_axis": 200, "semi_minor_axis": 90, "color": RED, "speed": 47},
    {"name": "Venus", "radius": 18, "semi_major_axis": 300, "semi_minor_axis": 140, "color": ORANGE_T1, "speed": 35},
    {"name": "Earth", "radius": 19, "semi_major_axis": 400, "semi_minor_axis": 190, "color": BLUE, "speed": 29.78},
    {"name": "Mars", "radius": 17, "semi_major_axis": 500, "semi_minor_axis": 240, "color": ORANGE_TO, "speed": 24},
    {"name": "Jupiter", "radius": 30, "semi_major_axis": 600, "semi_minor_axis": 290, "color": YELLOW2, "speed": 13},
    {"name": "Saturne", "radius": 70, "semi_major_axis": 700, "semi_minor_axis": 340, "color": BROWN, "speed": 9.68},
    {"name": "Uranus", "radius": 35, "semi_major_axis": 800, "semi_minor_axis": 390, "color": BLUE, "speed": 7},
    {"name": "Neptune", "radius": 30, "semi_major_axis": 900, "semi_minor_axis": 440, "color": BLUE, "speed": 5.4},
]

# Paramètres des anneaux de Saturne
SATURN_RING_COLOR = (200, 200, 200)  # Couleur grise pour les anneaux de Saturne
SATURN_RING_INNER_RADIUS = 40
SATURN_RING_OUTER_RADIUS = 48

# Création de la fenêtre
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Système Solaire")

clock = pygame.time.Clock()

# Chargement de l'image de texture du soleil
sun_texture = pygame.image.load(SUN_TEXTURE_PATH).convert()
# Redimensionnement de l'image de texture pour correspondre au rayon du soleil
sun_texture = pygame.transform.scale(sun_texture, (2 * SUN_RADIUS, 2 * SUN_RADIUS))

# Chargement de l'image de texture de la Terre
earth_texture = pygame.image.load(EARTH_TEXTURE_PATH).convert()
# Redimensionnement de l'image de texture pour correspondre au rayon de la Terre
earth_texture = pygame.transform.scale(earth_texture, (2 * EARTH_RADIUS, 2 * EARTH_RADIUS))

# Chargement des images de texture des autres planètes
planet_textures = {}
for planet_name, texture_path in PLANET_TEXTURE_PATHS.items():
    texture = pygame.image.load(texture_path).convert_alpha()
    # Redimensionnement de l'image de texture pour correspondre au rayon de la planète
    texture = pygame.transform.scale(texture, (2 * PLANET_DATA[PLANET_DATA.index(next(p for p in PLANET_DATA if p["name"] == planet_name))]["radius"], 2 * PLANET_DATA[PLANET_DATA.index(next(p for p in PLANET_DATA if p["name"] == planet_name))]["radius"]))
    planet_textures[planet_name] = texture

# Variable de contrôle de la vitesse du temps
TIME_MULTIPLIER = 1.0
PAUSE = False

# Étapes de ralentissement et d'accélération
SLOW_STEP = 1 / 2
SPEED_STEP = 2

# Classe pour représenter une étoile filante
class ShootingStar:
    def __init__(self, x, y, speed, is_static):
        self.x = x
        self.y = y
        self.speed = speed
        self.is_static = is_static

    def move(self):
        if not self.is_static:
            self.y += self.speed

# Génération aléatoire des étoiles filantes
def generate_shooting_stars(num_stars, screen_width, screen_height):
    stars = []
    for _ in range(num_stars):
        x = random.randint(0, screen_width)
        y = random.randint(0, screen_height)
        speed = random.uniform(1, 4)
        is_static = random.choice([True, False])
        stars.append(ShootingStar(x, y, speed, is_static))
    return stars

# Dessiner les étoiles filantes
def draw_shooting_stars(stars, screen):
    for star in stars:
        pygame.draw.line(screen, WHITE, (star.x, star.y), (star.x, star.y + 1))

# Mouvement des étoiles filantes
def move_shooting_stars(stars):
    for star in stars:
        star.move()

# Fonction pour dessiner les trajectoires des planètes
def draw_orbits():
    for planet in PLANET_DATA[1:]:
        semi_major_axis = planet
        semi_major_axis = planet["semi_major_axis"]
        semi_minor_axis = planet["semi_minor_axis"]
        orbit_rect = pygame.Rect(SUN_POS[0] - semi_major_axis, SUN_POS[1] - semi_minor_axis, 2 * semi_major_axis, 2 * semi_minor_axis)
        pygame.draw.ellipse(screen, WHITE, orbit_rect, 2)  # Épaisseur de la ligne augmentée à 3

# Fonction pour dessiner les objets
def draw_objects():
    for planet in PLANET_DATA:
        if planet["name"] == "Sun":
            # Dessiner la texture du soleil à la position du soleil
            screen.blit(sun_texture, (SUN_POS[0] - SUN_RADIUS, SUN_POS[1] - SUN_RADIUS))
        else:
            planet_x = SUN_POS[0] + planet["semi_major_axis"] * math.cos(math.radians(planet["angle"]))
            planet_y = SUN_POS[1] + planet["semi_minor_axis"] * math.sin(math.radians(planet["angle"]))
            planet_pos = (int(planet_x), int(planet_y))
            if planet["name"] in planet_textures:
                # Dessiner la texture de la planète à sa position
                screen.blit(planet_textures[planet["name"]], (planet_pos[0] - PLANET_DATA[PLANET_DATA.index(planet)]["radius"], planet_pos[1] - PLANET_DATA[PLANET_DATA.index(planet)]["radius"]))
            else:
                pygame.draw.circle(screen, planet["color"], planet_pos, planet["radius"])

# Fonction pour ralentir le temps
def slow_time():
    global TIME_MULTIPLIER
    TIME_MULTIPLIER *= SLOW_STEP
    # Limiter le multiplicateur de temps
    TIME_MULTIPLIER = max(0.125, TIME_MULTIPLIER)

# Fonction pour accélérer le temps
def speed_up_time():
    global TIME_MULTIPLIER
    TIME_MULTIPLIER *= SPEED_STEP
    # Limiter le multiplicateur de temps
    TIME_MULTIPLIER = min(8, TIME_MULTIPLIER)

# Fonction pour mettre le temps en pause
def toggle_pause():
    global PAUSE
    PAUSE = not PAUSE

# Fonction pour déterminer si un point se trouve dans un rectangle
def point_in_rect(point, rect):
    x, y = point
    rx, ry, rw, rh = rect
    return rx < x < rx + rw and ry < y < ry + rh

# Création des boutons
BUTTON_SIZE = 65
BUTTON_MARGIN = 10
BUTTON_COLOR = (0, 0, 0)
BUTTON_FONT = pygame.font.SysFont(None, 30)

def draw_button(text, position, action):
    rect = pygame.Rect(position[0], position[1], BUTTON_SIZE, BUTTON_SIZE)
    pygame.draw.rect(screen, BUTTON_COLOR, rect)
    text_surface = BUTTON_FONT.render(text, True, WHITE)
    text_rect = text_surface.get_rect(center=rect.center)
    screen.blit(text_surface, text_rect)
    if action == "pause":
        return rect  # Retourne le rectangle du bouton pause pour la détection de clic
    elif action == "slow":
        return rect  # Retourne le rectangle du bouton slow pour la détection de clic
    elif action == "speed":
        return rect  # Retourne le rectangle du bouton speed pour la détection de clic

# Positionnement des boutons et du texte
PAUSE_BUTTON_POS = (BUTTON_MARGIN, BUTTON_MARGIN)
SLOW_BUTTON_POS = (BUTTON_MARGIN, BUTTON_MARGIN * 2 + BUTTON_SIZE)
SPEED_BUTTON_POS = (BUTTON_MARGIN, BUTTON_MARGIN * 3 + BUTTON_SIZE * 2)
CURRENT_SPEED_POS = (BUTTON_MARGIN + BUTTON_SIZE * 3 + BUTTON_MARGIN * 2, BUTTON_MARGIN)

# Boucle principale
def main():
    for planet in PLANET_DATA:
        planet["angle"] = 0

    # Générer les étoiles filantes
    shooting_stars = generate_shooting_stars(50, WIDTH, HEIGHT)

    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_DOWN:
                    slow_time()
                elif event.key == pygame.K_UP:
                    speed_up_time()
                elif event.key == pygame.K_SPACE:
                    toggle_pause()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if point_in_rect(event.pos, draw_button("Pause", PAUSE_BUTTON_POS, "pause")):
                    toggle_pause()
                elif point_in_rect(event.pos, draw_button("Slow", SLOW_BUTTON_POS, "slow")):
                    slow_time()
                elif point_in_rect(event.pos, draw_button("Speed", SPEED_BUTTON_POS, "speed")):
                    speed_up_time()
                elif PAUSE:  # Ajoutez cette condition pour autoriser le clic sur les planètes uniquement en pause
                    for planet in PLANET_DATA[1:]:
                        planet_x = SUN_POS[0] + planet["semi_major_axis"] * math.cos(math.radians(planet["angle"]))
                        planet_y = SUN_POS[1] + planet["semi_minor_axis"] * math.sin(math.radians(planet["angle"]))
                        planet_pos = (planet_x, planet_y)
                        distance = math.sqrt((event.pos[0] - planet_pos[0])**2 + (event.pos[1] - planet_pos[1])**2)
                        if distance < planet["radius"]:
                            if planet["name"] == "Sun":
                                show_planet_info("Sun")  # Afficher les informations du Soleil
                            else:
                                show_planet_info(planet["name"])

        screen.fill((0, 0, 0))

        # Dessiner les étoiles filantes
        draw_shooting_stars(shooting_stars, screen)

        # Dessiner le reste des objets du système solaire
        draw_orbits()
        draw_objects()

        pygame.draw.rect(screen, (0, 0, 0), draw_button("Pause", PAUSE_BUTTON_POS, "pause"), 3)
        pygame.draw.rect(screen, (0, 0, 0), draw_button("Slow", SLOW_BUTTON_POS, "slow"), 3)
        pygame.draw.rect(screen, (0, 0, 0), draw_button("Speed", SPEED_BUTTON_POS, "speed"), 3)

        if not PAUSE:
            for planet in PLANET_DATA[1:]:
                planet["angle"] += planet["speed"] * TIME_MULTIPLIER * (360 / (2 * math.pi * max(planet["semi_major_axis"], planet["semi_minor_axis"])))

        # Affichage du palier de temps actuel
        current_speed_text = f"Current Speed: x{TIME_MULTIPLIER}"
        current_speed_surface = BUTTON_FONT.render(current_speed_text, True, WHITE)
        current_speed_rect = current_speed_surface.get_rect(midtop=CURRENT_SPEED_POS)
        screen.blit(current_speed_surface, current_speed_rect)

        pygame.display.flip()
        clock.tick(FPS)

    pygame.quit()



# Fonction pour afficher les informations spécifiques à chaque planète
def show_planet_info(planet_name):
    info_text = ""
    
    if planet_name == "Sun":
        info_text = ("Le Soleil est l'étoile centrale du système solaire, autour de laquelle toutes les planètes orbitent.\n"
                     "Masse : 1.989 × 10^30 kg\n"
                     "Rayon : 695,700 km\n"
                     "Type : Naine jaune\n"
                     "Âge : 4,6 milliards d'années\n"
                     "Température de surface : 5,500 °C (9,932 °F)\n"
                     "Luminosité : 3,8 × 10^26 W\n"
                     "Distance au centre de la galaxie : environ 27,000 années-lumière\n"
                     "Durée d'une rotation : environ 25 à 35 jours à l'équateur\n"
                     "Durée de la révolution autour du centre galactique : environ 225-250 millions d'années")
    elif planet_name == "Mercure":
        info_text = ("Mercure est la planète la plus proche du Soleil et la plus petite du système solaire.\n"
                     "Masse : 3.301 × 10^23 kg\n"
                     "Rayon : 2439.7 km\n"
                     "Type : Rocheuse\n"
                     "Age : 4,503 milliards d'années\n"
                     "Superficie : 74,8 millions km²\n"
                     "Gravité : 3,70 m/s²\n"
                     "Distance au Soleil : 58 millions de km\n"
                     "Période orbitale : 88 jours\n"
                     "Durée du jour : 59 jours 0 heure 0 minute")
    elif planet_name == "Venus":
        info_text = ("Venus est la deuxième planète du système solaire en partant du Soleil.\n"
                     "Masse : 4.867 × 10^24 kg\n"
                     "Rayon : 6051.8 km\n"
                     "Type : Rocheuse\n"
                     "Age : 4,503 milliards d'années\n"
                     "Superficie : 460,2 millions km²\n"
                     "Gravité : 8,87 m/s²\n"
                     "Distance au Soleil : 108,2 millions km\n"
                     "Période orbitale : 225 jours\n"
                     "Durée du jour : 243 jours 0 heure 0 minute")
    elif planet_name == "Earth":
        info_text = ("La Terre est s’est formée il y a 4.54 milliard d'années. Elle possède un seul satellite naturel, la Lune.\n"
                     "Masse : 5.972 × 10^24 kg\n"
                     "Rayon : 6371 km\n"
                     "Type : Rocheuse\n"
                     "Age : 4,543 milliards d'années\n"
                     "Superficie : 510 millions km²\n"
                     "Gravité : 9,807 m/s²\n"
                     "Distance au Soleil : 149 millions de km\n"
                     "Période orbitale : 365 jours\n"
                     "Durée du jour : 1 jour")
    elif planet_name == "Mars":
        info_text = ("Mars est la quatrième planète du système solaire en partant du Soleil.\n"
                     "Masse : 6.39 × 10^23 kg\n"
                     "Rayon : 3389.5 km\n"
                     "Type : Rocheuse\n"
                     "Age : 4,603 milliards d'années\n"
                     "Superficie : 144 millions km²\n"
                     "Gravité : 3,71 m/s²\n"
                     "Distance au Soleil : 227,9 millions km\n"
                     "Période orbitale : 687 jours\n"
                     "Durée du jour : 1 jour 0 heure 37 minutes")
    elif planet_name == "Jupiter":
        info_text = ("Jupiter est la cinquième planète du système solaire en partant du Soleil.\n"
                     "Masse : 1.898 × 10^27 kg\n"
                     "Rayon : 69911 km\n"
                     "Type : Gazeuse\n"
                     "Age : 4,603 milliards d'années\n"
                     "Superficie : 61,42 milliards km²\n"
                     "Gravité : 24,79 m/s²\n"
                     "Distance au Soleil : 778,5 millions km\n"
                     "Période orbitale : 11,86 ans\n"
                     "Durée du jour : 0 jour 9 heures 56 minutes")
    elif planet_name == "Saturne":
        info_text = ("Saturne est la sixième planète du système solaire en partant du Soleil.\n"
                     "Masse : 5.683 × 10^26 kg\n"
                     "Rayon : 58232 km\n"
                     "Type : Gazeuse\n"
                     "Age : 4,503 milliards d'années\n"
                     "Superficie : 42,7 milliards km²\n"
                     "Gravité : 10,44 m/s²\n"
                     "Distance au Soleil : 1,434 milliard km\n"
                     "Période orbitale : 29 ans\n"
                     "Durée du jour : 0 jour 10 heures 34 minutes")
    elif planet_name == "Uranus":
        info_text = ("Uranus est la septième planète du système solaire en partant du Soleil.\n"
                     "Masse : 8.681 × 10^25 kg\n"
                     "Rayon : 25362 km\n"
                     "Type : Gazeuse\n"
                     "Age : 4,503 milliards d'années\n"
                     "Superficie : 8,083 milliards km²\n"
                     "Gravité : 8,87 m/s²\n"
                     "Distance au Soleil : 2,871 milliards km\n"
                     "Période orbitale : 84 ans\n"
                     "Durée du jour : 0 jour 17 heures 14 minutes")
    elif planet_name == "Neptune":
        info_text = ("Neptune est la huitième planète du système solaire en partant du Soleil.\n"
                     "Masse : 1.024 × 10^26 kg\n"
                     "Rayon : 24622 km\n"
                     "Type : Gazeuse\n"
                     "Age : 4,503 milliards d'années\n"
                     "Superficie : 7,618 milliards km²\n"
                     "Gravité : 11,15 m/s²\n"
                     "Distance au Soleil : 4,495 milliards km\n"
                     "Période orbitale : 165 ans\n"
                     "Durée du jour : 0 jour 16 heures 6 minutes")
    else:
        info_text = "Aucune information disponible pour cette planète."


    messagebox.showinfo(planet_name, info_text)


# Fonction pour ouvrir une fenêtre d'information
def show_info_window():
    info_text = """
    Vous pouvez utiliser espaces pour mettre pause, flèche du haut pour accélérer ou encore flèche du bas pour ralentir
    """
    messagebox.showinfo("Information", info_text)
    
def show_credit_window():
    info_text = """
    Projet par PHEULPIN Lucas, 
    RIVIERE Bixente et LAVAL--MONTFORT Eric.
    Eleve de Terminale SPE NSI
    Suivi par notre enseignant M. Baddaoui.
    
    INSTITUT NOTRE DAME, Chartres
    
    
    """
    messagebox.showinfo("Credit", info_text)



# Fonction pour afficher le menu principal
def main_menu():
    root = tk.Tk()
    root.geometry("1920x1080")  # Réglez la taille de la fenêtre
    root.title("Menu d'Initialisation")


    # Charger l'image de fond
    background_image = tk.PhotoImage(file="R.png")

    # Création du widget Label pour afficher l'image de fond
    background_label = tk.Label(root, image=background_image)
    background_label.place(relwidth=1, relheight=1)  # Faire en sorte que l'image couvre toute la fenêtre

    
    
    # Fonction pour démarrer le système solaire
    def start_solar_system():
        root.destroy()  # Ferme la fenêtre du menu principal
        main()  # Lance le système solaire


# Création du titre
    title_label = tk.Label(root, text="AstroPy", font=("Arial", 75))
    title_label.pack(pady=(50, 20))  # Ajustez la marge verticale en haut du titre en pixels 

# Création des boutons
    start_button = tk.Button(root, text="Start", command=start_solar_system)
    start_button.pack(pady=10)
    start_button.config(width=10, height=1)  # Ajustez la largeur et la hauteur du bouton Start
    start_button.place(relx=0.7, rely=0.37, anchor=tk.CENTER)  # Centre le bouton horizontalement
    
    info_button = tk.Button(root, text="Information", command=show_info_window)
    info_button.pack(pady=10)
    info_button.config(width=15, height=1)  # Ajustez la largeur et la hauteur du bouton Information
    info_button.place(relx=0.2, rely=0.7, anchor=tk.CENTER)  # Centre le bouton horizontalement
    
    credit_button = tk.Button(root, text="Credit", command=show_credit_window)
    credit_button.pack(pady=10)
    credit_button.config(width=10, height=1)  # Ajustez la largeur et la hauteur du bouton Information
    credit_button.place(relx=0.135, rely=0.08, anchor=tk.CENTER)  # Centre le bouton horizontalement

    root.mainloop()

# Appel de la fonction pour afficher le menu principal au démarrage du programme
if __name__ == "__main__":
    main_menu()
