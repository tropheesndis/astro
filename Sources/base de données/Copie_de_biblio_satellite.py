satelite_jupiter={
    "Metis":{
        "label":"XVI",
        "ordre":1,
        "Désignation_provisoire":"S/1979 J 3",
        "diamètre": 43,
        "masse":10*10**16,
        "demi-grand_axe": 128000,
        "période_orbital":0.29478,
        "période_rotation":0.29826,
        "inclinaison":0.019,
        "excentricité":0.0012,
        "masse_vol_moy":0.86*10**3,
        "gravité": 0.005,
        "temp_surf":123,
        "albédo": 0.061+0.003,
        "press_atmosp":None,
        "groupe":"Amathée",
        "decouverte":"1979",
        "decouvreur":"S.Synnot",
         },
    "Adrastée":{
        "label":"XV",
        "ordre": 2,
        "Désignation_provisoire":"S/1979 J 1",
        "diamètre": 26*16*14**5,
        "masse":2*10**15,
        "demi-grand_axe": 129000,
        "période_orbital":0.29826 ,
        "période_rotation":0.29826,
        "inclinaison":0.054,
        "excentricité":0.0018,
        "masse_vol_moy":0.86*10**3,
        "gravité":0.002,
        "temp_surf":122,
        "albédo": 0.1+0.045,
        "press_atmosp":None,
        "groupe":"Amathée",
        "decouverte":"8 juillet 1979",
        "decouvreur":"D. C. Jewitt, G. E. Danielson",
        
        
         },
     "Amalthée":{
        "label":"V",
        "ordre": 3,
        "Désignation_provisoire":"None",
        "diamètre": 167**3,
        "masse":2.08+0.15*10**18,
        "demi-grand_axe": 181365.84+0.02,
        "période_orbital":0.49817943 ,
        "période_rotation":0.49817943,
        "inclinaison":0.388,
        "excentricité":0.0031,
        "masse_vol_moy":0.857+0.099*10**3,
         },
    
    
    
    
    
    }   