planets = {

    "Terre": {

        "Masse": "5.972 × 10^24 kg",

        "Rayon": 6371,

        "Type": "Rocheuse",

        "Age": "4,543 milliards",

        "Superficie": "510 millions km²",

        "Gravité": "9,807 m/s²",

        "Distance Soleil": "149 millions de km",

        "Période orbitale": "365jours",

        "Durée du jour": "1j"

    },

    "Mars": {

        "Masse": "6.39 × 10^23 kg",

        "Rayon": 3389.5,

        "Type": "Rocheuse",

        "Age": "4,603 milliards",

        "Superficie": "144 millions km²",

        "Gravité": "3,71 m/s²",

        "Distance Soleil": "227,9 millions km",

        "Période orbitale": "687 jours",

        "Durée du jour": "1j 0h 37m"

    },

    "Venus": {

        "Masse": "4.867 × 10^24 kg",

        "Rayon": 6051.8,

        "Type": "Rocheuse",

        "Age": "4,503 milliards",

        "Superficie": "460,2 millions km²",

        "Gravité": "8,87 m/s²",

        "Distance Soleil": "108,2 millions km",

        "Période orbitale": "225 jours",

        "Durée du jour": "243j 0h 0m"

    },

    "Jupiter": {

        "Masse": "1.898 × 10^27 kg",

        "Rayon": 69911,

        "Type": "Gazeuse",

        "Age": "4,603 milliards",

        "Superficie": "61,42 milliards km²",

        "Gravité": "24,79 m/s²",

        "Distance Soleil": "778,5 millions km",

        "Période orbitale": "11,86 ans",

        "Durée du jour": "0j 9h 56m"

    },

    "Mercure": {

        "Masse": "3.301 1 × 1023 kg",

        "Rayon": 2439.7,

        "Type": "Rocheuse",

        "Age": "4,503 milliards",

        "Superficie": "74,8 millions km2",

        "Gravité": "3,70 m/s²",

        "Distance Soleil": "58 millions km",

        "Période orbitale": "88 jours",

        "Durée du jour": "59j 0h 0m"

    },

    "Uranus": {

        "Masse": "8.681 × 10^25 kg",

        "Rayon": 25362,

        "Type": "Gazeuse",

        "Age": "4,503 milliards",

        "Superficie": "8,083 milliards km²",

        "Gravité": "8,87 m/s²",

        "Distance Soleil": "2,871 milliards km",

        "Période orbitale": "84 ans",

        "Durée du jour": "0j 17h 14m"

    },

    "Neptune": {

        "Masse": "1.024 × 10^26 kg",

        "Rayon": 24622,

        "Type": "Gazeuse",

        "Age": "4,503 milliards",

        "Superficie": "7,618 milliards km²",

        "Gravité": "11,15 m/s²",

        "Distance Soleil": "4,495 milliards km",

        "Période orbitale": "165 ans",

        "Durée du jour": "0j 16h 6m"

    },

    "Saturne": {

        "Masse": "5.683 × 10^26 kg",

        "Rayon": 58232,

        "Type": "Gazeuse",

        "Age": "4,503 milliards",

        "Superficie": "42,7 milliards km²",

        "Gravité": "10,44 m/s²",

        "Distance Soleil": "1,434 milliard km",

        "Période orbitale": "29 ans",

        "Durée du jour": "0j 10h 34m"

    }

}