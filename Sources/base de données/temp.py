system_solar = {

    "Terre": {"masse":5.972 × 10^24 , 
              "rayon":6371,
              "type":"rocheuse",
              "age": 4.453,
              "superficie":510,
              "gravité":9.807,
              "distance_soleil":149,
              "periode_orbitale":365
              "durée_d_jour":1
              }
    
   "Mars": {"masse":6.39 × 10^23 , 
             "rayon":3389.5,
             "type":"rocheuse",
             "age": 4.603,
             "superficie":144,
             "gravité":3.71,
             "distance_soleil":227.9,
             "periode_orbitale":678,
             "durée_d_jour":1.0257,
             }
    
    "Venus": {"masse":4.867 × 10^24  , 
              "rayon":6051.8,
              "type":"rocheuse",
              "age": 4.503,
              "superficie":460.2,
              "gravité":8.87,
              "distance_soleil":108.2,
              "periode_orbitale":225,
              "durée_d_jour":243,
              }
     "Jupiter": {"masse":1.898 × 10^27  , 
               "rayon":69911,
               "type":"gazeuze",
               "age": 4.603,
               "superficie":61420,
               "gravité":14.79,
               "distance_soleil":778.5,
               "periode_orbitale":4015,
               "durée_d_jour": 0.4139 ,
               }
          
     "Mercure": {"masse":3.301 1 × 1023  , 
                 "rayon":2439.7,
                 "type":"rocheuse",
                 "age": 4.503,
                 "superficie":74.8,
                 "gravité":3.70,
                 "distance_soleil":58,
                 "periode_orbitale":88,
                 "durée_d_jour": 59 ,
           }
     "Uranus": {"masse":8,681 × 10^25  , 
                 "rayon":25362,
                 "type":"gazeuze",
                 "age": 4.503,
                 "superficie":8083,
                 "gravité":8.87,
                 "distance_soleil":2871,
                 "periode_orbitale":30660,
                 "durée_d_jour":  0.7181 ,
                 "Sattelitte"={
    "Cordélia":{
        "Ordre":1,
        "Label":"VI",
        "diamètre":46.2,
        "masse":0.044,
        "demigrandaxe":49751,
        "Periodeorb":0.335034,
        "Inclinaison":0.08479,
        "Exentricité":0.00026,
        "Decouverte":1986,
        "Decouvreur":"Terrile",
        },
    "Ophélie":{
        "Ordre":2,
        "Label":"VII",
        "diamètre":50.8,
        "masse":0.053,
        "demigrandaxe":53764,
        "Periodeorb":0.3464,
        "Inclinaison":0.1036,
        "Exentricité":0.00992,
        "Decouverte":1986,
        "Decouvreur":"Terrile",
        },
    "Bianca":{
        "Ordre":3,
        "Label":"VIII",
        "diamètre":55.4,
        "masse":0.092,
        "demigrandaxe":59165,
        "Periodeorb":0.434579,
        "Inclinaison":0.193,
        "Exentricité":0.00092,
        "Decouverte":1986,
        "Decouvreur":"Smith",
        },
     "Cressida":{
        "Ordre":4,
        "Label":"IX",
        "diamètre":83.6,
        "masse":0.34,
        "demigrandaxe":61799,
        "Periodeorb":0.46365,
        "Inclinaison":0.006,
        "Exentricité":0.00036,
        "Decouverte":1986,
        "Decouvreur":"Synnott",
        },
      "Desdémone":{
        "Ordre":5,
        "Label":"X",
        "diamètre":72.0,
        "masse":0.18,
        "demigrandaxe":62658,
        "Periodeorb":0.47365,
        "Inclinaison":0.11125,
        "Exentricité":0.00036,
        "Decouverte":1986,
        "Decouvreur":"Synnott",
        },
       "Juliette":{
        "Ordre":6,
        "Label":"XI",
        "diamètre":101.6,
        "masse":0.56,
        "demigrandaxe":64360,
        "Periodeorb":0.493065,
        "Inclinaison":0.065,
        "Exentricité":0.00066,
        "Decouverte":1986,
        "Decouvreur":"Synnott",
        },
       "Portia":{
        "Ordre":7,
        "Label":"XII",
        "diamètre":148.2,
        "masse":1.7,
        "demigrandaxe":66097,
        "Periodeorb":0.513196,
        "Inclinaison":0.059,
        "Exentricité":0.00005,
        "Decouverte":1986,
        "Decouvreur":"Synnott",
        },
       "Rosalinde":{
        "Ordre":8,
        "Label":"XIII",
        "diamètre":84,
        "masse":0.25,
        "demigrandaxe":69927,
        "Periodeorb":0.55846,
        "Inclinaison":0.279,
        "Exentricité":0.00011,
        "Decouverte":1986,
        "Decouvreur":"Synnott",
        },
       "Cupid":{
        "Ordre":9,
        "Label":"XXVII",
        "diamètre":18,
        "masse":0.0038,
        "demigrandaxe":74800,
        "Periodeorb":0.618,
        "Inclinaison":0.1,
        "Exentricité":0.0013,
        "Decouverte":2003,
        "Decouvreur":"Showalter et Lissaue",
        },
      "Belinda":{
        "Ordre":10,
        "Label":"XIV",
        "diamètre":106,
        "masse":0.49,
        "demigrandaxe":75255,
        "Periodeorb":0.623527,
        "Inclinaison":0.031,
        "Exentricité":0.00007,
        "Decouverte":1986,
        "Decouvreur":"Synnott",
        },
      "Belinda":{
        "Ordre":10,
        "Label":"XIV",
        "diamètre":106,
        "masse":0.49,
        "demigrandaxe":75255,
        "Periodeorb":0.623527,
        "Inclinaison":0.031,
        "Exentricité":0.00007,
        "Decouverte":1986,
        "Decouvreur":"Synnott",
        },
      "Perdita":{
        "Ordre":11,
        "Label":"XXV",
        "diamètre":36,
        "masse":0.018,
        "demigrandaxe":76420,
        "Periodeorb":0.638,
        "Inclinaison":0,
        "Exentricité":0.0012,
        "Decouverte":1986,
        "Decouvreur":"Karkoschaka",
        },
       "Puck":{
        "Ordre":12,
        "Label":"XV",
        "diamètre":166,
        "masse":2.9,
        "demigrandaxe":86004,
        "Periodeorb":0.761833,
        "Inclinaison":0.3192,
        "Exentricité":0.00012,
        "Decouverte":1956,
        "Decouvreur":"Synnott",
        },
        "Mab":{
        "Ordre":13,
        "Label":"XXVI",
        "diamètre":25,
        "masse":0.01,
        "demigrandaxe":97734,
        "Periodeorb":0.923,
        "Inclinaison":0.1335,
        "Exentricité":0.0025,
        "Decouverte":2003,
        "Decouvreur":"Showalter et Lissaue",
        },
        "‡Miranda":{
        "Ordre":14,
        "Label":"V",
        "diamètre":473,
        "masse":67,
        "demigrandaxe":129390,
        "Periodeorb":1.413479,
        "Inclinaison":4.232,
        "Exentricité":0.0013,
        "Decouverte":1948,
        "Decouvreur":"Kuiper",
        },
        "‡Ariel":{
        "Ordre":15,
        "Label":"I",
        "diamètre":1159,
        "masse":1470,
        "demigrandaxe":191020,
        "Periodeorb":2.520379,
        "Inclinaison":0.260,
        "Exentricité":0.0012,
        "Decouverte":1851,
        "Decouvreur":"Lassell",
        },
        "‡Umbriel":{
        "Ordre":16,
        "Label":"II",
        "diamètre":1175,
        "masse":1300,
        "demigrandaxe":266300,
        "Periodeorb":4.144177,
        "Inclinaison":0.205,
        "Exentricité":0.0,
        "Decouverte":1851,
        "Decouvreur":"Lassell",
        },
         "‡Titania":{
        "Ordre":17,
        "Label":"III",
        "diamètre":1580,
        "masse":3620,
        "demigrandaxe":435910,
        "Periodeorb":8.705872,
        "Inclinaison":0.340,
        "Exentricité":0.0011,
        "Decouverte":1787,
        "Decouvreur":"Herschel",
        },
        "‡Obéron":{
        "Ordre":18,
        "Label":"IV",
        "diamètre":1528,
        "masse":3070,
        "demigrandaxe":583520,
        "Periodeorb":13.463239,
        "Inclinaison":0.058,
        "Exentricité":0.0014,
        "Decouverte":1787,
        "Decouvreur":"Herschel",
        },
        "♠Francisco":{
        "Ordre":19,
        "Label":"XXII",
        "diamètre":22,
        "masse":0.0072,
        "demigrandaxe":4276000,
        "Periodeorb": −266.56,
        "Inclinaison":147.459,
        "Exentricité":0.1459,
        "Decouverte":2001,
        "Decouvreur":"Herschel",
        },
         "♠Caliban":{
        "Ordre":20,
        "Label":"XVI",
        "diamètre":72,
        "masse":0.25,
        "demigrandaxe":7231000,
        "Periodeorb": −579.73,
        "Inclinaison":139.885,
        "Exentricité":0.1587,
        "Decouverte":1997,
        "Decouvreur":"Gladman et al.",
        },
         "♠Stephano":{
        "Ordre":21,
        "Label":"XX",
        "diamètre":32,
        "masse":0.022,
        "demigrandaxe":8004000,
        "Periodeorb":−677.37,
        "Inclinaison":141873,
        "Exentricité":0.2292,
        "Decouverte":1999,
        "Decouvreur":"Gladman et al.",
        },
         "♠Trinculo":{
        "Ordre":22,
        "Label":"XX",
        "diamètre":18,
        "masse":0.0039,
        "demigrandaxe":8504000,
        "Periodeorb":−749.24,
        "Inclinaison":166.252,
        "Exentricité":0.22,
        "Decouverte":2001,
        "Decouvreur":"Holman et al.",
        },
         "♠Sycorax":{
        "Ordre":23,
        "Label":"XVII",
        "diamètre":150,
        "masse":2.3,
        "demigrandaxe":12179000,
        "Periodeorb":−1288.28,
        "Inclinaison":152.456,
        "Exentricité":0.5224,
        "Decouverte":1997,
        "Decouvreur":"Nicholson et al.",
        },
         "Margaret":{
        "Ordre":24,
        "Label":"XXIII",
        "diamètre":20,
        "masse":0.0054,
        "demigrandaxe":14345000,
        "Periodeorb":1687.01,
        "Inclinaison":51.455,
        "Exentricité":0.6608,
        "Decouverte":2003,
        "Decouvreur":"Sheppard et Jewitt",
        },
         "♠Prospero":{
        "Ordre":25,
        "Label":"XVIII",
        "diamètre":50,
        "masse":0.085,
        "demigrandaxe":16256000,
        "Periodeorb":−1978.29,
        "Inclinaison":146.017,
        "Exentricité":0.4448,
        "Decouverte":1999,
        "Decouvreur":"Holman et al.",
        },
          "♠Setebos":{
        "Ordre":26,
        "Label":"XXIX",
        "diamètre":48,
        "masse":0.075,
        "demigrandaxe":17418000,
        "Periodeorb":−2225.21,
        "Inclinaison":145.883,
        "Exentricité":0.5914,
        "Decouverte":1999,
        "Decouvreur":"Kavelaars et al.",
        },
          "♠Ferdinand":{
        "Ordre":76,
        "Label":"XXIV",
        "diamètre":20,
        "masse":0.0054,
        "demigrandaxe":20901000,
        "Periodeorb":−2805.51,
        "Inclinaison":167.346,
        "Exentricité":0.3682,
        "Decouverte":2001,
        "Decouvreur":"Holman et al.",
        },
         
         
        
         
        
       
       
       
      
    
    }


           }
    
    }

